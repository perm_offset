#!/usr/bin/python3

import argparse
import logging
import os
from stat import S_IMODE, S_ISLNK
import sys

def remap_file(fpath, src, dst, count, dry_run):
    stat = os.lstat(fpath)

    do_remap = False

    uid = stat.st_uid
    if uid >= src and uid < src + count:
        uid += dst - src
        do_remap = True
    gid = stat.st_gid
    if gid >= src and gid < src + count:
        gid += dst - src
        do_remap = True

    if not do_remap:
        return

    mode = S_IMODE(stat.st_mode)

    logging.debug('chown(%s, mode=%o %d->%d, %d->%d)' %
                  (fpath, mode, stat.st_uid, uid, stat.st_gid, gid))
    if dry_run:
        return

    if S_ISLNK(stat.st_mode) and not os.chown in os.supports_follow_symlinks:
        logging.debug('Skipping link, os.chown() does not support symlinks: %s', fpath)
        return
    os.chown(fpath, uid, gid, follow_symlinks = False)

    # chown may clear setuid bits, so make sure to preserve them
    if S_ISLNK(stat.st_mode) and not os.chmod in os.supports_follow_symlinks:
        logging.debug('Skipping link, os.chmod() does not support symlinks: %s', fpath)
        return
    os.chmod(fpath, mode, follow_symlinks = False)

parser = argparse.ArgumentParser(
    description = "Remap UIDs/GIDs in a directory tree from one range to another"
)

parser.add_argument('-c', '--count', type = int, default = 65536,
                    help = 'Number of IDs in range')
parser.add_argument('-n', '--dry-run', action = 'store_true')
parser.add_argument('-v', '--verbose', action = 'count', default = 0)

parser.add_argument('src_start', type = int, help = 'First ID of the range to remap from')
parser.add_argument('dst_start', type = int, help = 'First ID of the range to remap to')
parser.add_argument('targets', nargs = '+')

args = parser.parse_args(sys.argv[1:])

logging.basicConfig(level = max(3 - args.verbose, 0) * 10)

for tgt in args.targets:
    logging.info('Processing target: %s' % tgt)

    for dirpath, dirnames, filenames in os.walk(tgt):
        logging.debug('At directory: %s' % dirpath)

        remap_file(dirpath, args.src_start, args.dst_start, args.count, args.dry_run)

        for fn in dirnames + filenames:
            remap_file(os.path.join(dirpath, fn), args.src_start,
                       args.dst_start, args.count, args.dry_run)
